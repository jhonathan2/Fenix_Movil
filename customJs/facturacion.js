$(document).on("ready", facturacion);

function facturacion()
{

  $("#btnCamerFacturacion").on("click", abrirCamara_Facturacion);
  $("#btnCameraArchivoFacturacion").on("click", abrirArchivo_Facturacion);

  $("#grpFacturacionFechaInicio input").datepicker(
    {
      changeMonth: true,
      changeYear: true
    });

   $("#grpFacturacionFechaInicio .add-on").on("click", function()
    {
      $("#grpFacturacionFechaInicio input").datepicker("show");
    });

   $('#tabsFacturacion .timepicker-24').timepicker({
      minuteStep: 1,
      showSeconds: false,
      showMeridian: false,
      disableMousewheel : true,
      disableFocus: true
  });

  $("#txtFacturacion_FechaInicio").on("change", consultarFacturacion);

   /*
   var f = new Date();
  $("#txtFacturacion_FechaInicio").val(f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-" + CompletarConCero(f.getDate(), 2));
  $("#txtFacturacion_HoraIni").val( CompletarConCero(f.getHours(), 2) + ":" + CompletarConCero(f.getMinutes(), 2));
  */

   $("#tabsFacturacion .factRadio").buttonset();

   $("#tabsFacturacion .chosen").chosen();
  
   $("#frmFacturacion").on("submit", function(evento)
    {
      evento.preventDefault();
      var pPrefijo = $('#txtFacturacion_FechaInicio').val().replace(/-/g, "");
      pPrefijo = CompletarConCero(Usuario.id, 3) + pPrefijo;

      ejecutarSQL("SELECT * FROM Facturacion WHERE Prefijo = ?", [pPrefijo], 
      function(Inspecciones)
      {
        var datos = {};
        if (Inspecciones.length > 0)
        {
          datos = [$('#txtFacturacion_FechaInicio').val(), $('#txtFacturacion_HoraIni').val(), $('#txtFacturacion_HoraFin').val(), $('#txtFacturacion_Colaboradora').val(), $('#txtFacturacion_Municipio').val(), $('#txtFacturacion_Subzona').val(), $('#txtFacturacion_Supervisor').val(), $('#txtFacturacion_SupervisorCedula').val(), $('#txtFacturacion_HoraLlegada').val(), $('#txtFacturacion_HoraSalida').val(),  $('input:radio[name=radFacturacion]:checked').val(), $('#txtFacturacion_CharlaTema').val(), $('#txtFacturacion_ObservacionesGenerales').val(), pPrefijo];
          ejecutarInsert("UPDATE Facturacion SET Fecha = ?, HoraIni = ?, HoraFin = ?, Colaboradora = ?, Municipio = ?, SubZona = ?, Supervisor = ?, SupervisorCedula = ?, HoraLlegada = ?, HoraSalida = ?, Charla = ?, CharlaTema = ?, ObservacionesGenerales = ? WHERE Prefijo = ?", datos); 
        } else
        {
          datos = [Usuario.id, pPrefijo, $('#txtFacturacion_FechaInicio').val(), $('#txtFacturacion_HoraIni').val(), $('#txtFacturacion_HoraFin').val(), $('#txtFacturacion_Colaboradora').val(), $('#txtFacturacion_Municipio').val(), $('#txtFacturacion_Subzona').val(), $('#txtFacturacion_Supervisor').val(), $('#txtFacturacion_SupervisorCedula').val(), $('#txtFacturacion_HoraLlegada').val(), $('#txtFacturacion_HoraSalida').val(),  $('input:radio[name=radFacturacion]:checked').val(), $('#txtFacturacion_CharlaTema').val(), $('#txtFacturacion_ObservacionesGenerales').val()];
          ejecutarInsert("INSERT INTO Facturacion (idLogin, Prefijo, Fecha, HoraIni, HoraFin, Colaboradora, Municipio, SubZona, Supervisor, SupervisorCedula, HoraLlegada, HoraSalida, Charla, CharlaTema, ObservacionesGenerales) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", datos);
        }
        Mensaje("Ok", "La Ejecución ha sido almacenada.");
      });
    });

    $("#frmFacturacionAuditoria").on("submit", function(evento)
      {
        evento.preventDefault();
        var pPrefijo = $('#txtFacturacion_FechaInicio').val().replace(/-/g, "");
        pPrefijo = CompletarConCero(Usuario.id, 3) + pPrefijo;

        var datos = [$('input:radio[name=radFacturacionFormSup]:checked').val(), $('#txtFacturacionAuditoria_Cuadrillas').val(), $('#txtFacturacionAuditoria_Inspecciones').val(), $('input:radio[name=radFacturacionPlanSup]:checked').val(), $('#txtFacturacionAuditoria_Observaciones').val(), pPrefijo];
        ejecutarSQL("SELECT * FROM Facturacion WHERE Prefijo = ?", [pPrefijo], 
        function(Inspecciones)
        { 
          if (Inspecciones.length > 0)
          {
            ejecutarInsert("UPDATE Facturacion SET FormatoSup = ?, CuadrillasACargo = ?, InspeccionesRealizadas = ?, CumplePlanSup = ?, Observaciones = ? WHERE Prefijo = ?", datos);
            Mensaje("Ok", "La Auditoría fue Guardada.");   
          } else
          {
            Mensaje("Error", "No se almacenó la Auditoría, Hay que guardar primero la Información General.");
          }
        });
      });

  $("#frmFacturacionTerreno").on("submit", function(evento)
    {
      evento.preventDefault();
      var pPrefijo = $('#txtFacturacion_FechaInicio').val().replace(/-/g, "");
      pPrefijo = CompletarConCero(Usuario.id, 3) + pPrefijo;

      ejecutarSQL("SELECT * FROM Facturacion WHERE Prefijo = ?", [pPrefijo], 
      function(Inspecciones)
      {
        var datos = [Usuario.id, pPrefijo, $("#txtPrefijo").val(), $('#txtFacturacionTerreno_NumCuenta').val(), $('#txtFacturacionTerreno_NumMedidor').val(), $('#txtFacturacionTerreno_Sucursal').val(), $('#txtFacturacionTerreno_Direccion').val(), $('#txtFacturacionTerreno_Barrio').val(), $('#txtFacturacionTerreno_Ciclo').val(), $('#txtFacturacionTerreno_Grupo').val(), $('#txtFacturacionTerreno_Lectura').val(), $('input:radio[name=radFacturacionAnomalia]:checked').val(),  $('input:radio[name=radFacturacionEntregaFact]:checked').val(), $('input:radio[name=radFacturacionEntregaOpor]:checked').val(), $('input:radio[name=radFacturacionCumple]:checked').val(), $('#txtFacturacionTerreno_Observaciones').val()];
        if (Inspecciones.length > 0)
        {
          ejecutarInsert("INSERT INTO FacturacionTerreno (idLogin, Prefijo, PrefijoIns, NumCuenta, NumMedidor, Sucursal, Direccion, Barrio, Ciclo, Grupo, Lectura, Anomalia, EntregaFact, EntregaOpor, Cumple, Observaciones) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", datos);
          Mensaje("Ok", "La Inspección fue Guardada.");   
          $("#modulo_facturacion").remove();
        } else
        {
          Mensaje("Error", "No se almacenó la Inspección, Hay que guardar primero la Información General.");
        }
      });
    });
}
function consultarFacturacion()
{
  var pPrefijo = $('#txtFacturacion_FechaInicio').val().replace(/-/g, "");
      pPrefijo = CompletarConCero(Usuario.id, 3) + pPrefijo;

      var data = {};
  ejecutarSQL("SELECT * FROM Facturacion WHERE Prefijo = ?", [pPrefijo], 
  function(Inspecciones)
  {
    if (Inspecciones.length != 0)
      {
        data = Inspecciones[0];
        $("#txtFacturacion_HoraIni").val(data.HoraIni);
        $("#txtFacturacion_HoraFin").val(data.HoraFin);
        $("#txtFacturacion_Colaboradora").val(data.Colaboradora);
        $("#txtFacturacion_Municipio").val(data.Municipio);
        $("#txtFacturacion_Subzona").val(data.SubZona);
        $("#txtFacturacion_Supervisor").val(data.Supervisor);
        $("#txtFacturacion_SupervisorCedula").val(data.SupervisorCedula);
        $("#txtFacturacion_HoraLlegada").val(data.HoraLlegada);
        $("#txtFacturacion_HoraSalida").val(data.HoraSalida);
        if (data.Charla == "Si")
        {
          $("input:radio[name=radFacturacion]")[0].checked = true;
        }
        if (data.Charla == "No")
        {
          $("input:radio[name=radFacturacion]")[1].checked = true;
        }
        $("#txtFacturacion_Charla").buttonset("refresh");
        $("#txtFacturacion_CharlaTema").val(data.CharlaTema);
        $("#ObservacionesGenerales").val(data.ObservacionesGenerales);
      } else
      {
        Mensaje("Hey", "Recuerda guardar la Información general antes de Iniciar la Inspección.");
      }
  });
}

function abrirCamara_Facturacion(evento)
{
  evento.preventDefault();
  if ($("#txtFacturacion_FechaInicio").val() != "")
  {
    if ($("#txtFacturacionTerreno_Grupo").val() != "")
    {
      navigator.camera.getPicture(_Facturacion, cameraFail, 
        { quality: 80,
          sourceType: 1,
        destinationType: Camera.DestinationType.FILE_URI,
        saveToPhotoAlbum : true });
    }
    else
    {
      Mensaje("Error", "No ha diligenciado el grupo de facturación");
    }
  } else
  {
    Mensaje("Error", "No ha diligenciado la Fecha de la auditoría");
  }
}

function cameraSuccess_Facturacion(imageURI) 
{
    var pPrefijo = obtenerPrefijo();
    var publicPrefijo = $('#txtFacturacion_FechaInicio').val().replace(/-/g, "") + $("#txtFacturacionTerreno_Grupo").val();
    

    var tds = '<div class="span4">';
        tds += '<div class="thumbnail">';
          tds += '<div class="item">';
                tds += '<img src="" alt="Photo" id="image_' + pPrefijo + '"/>';
          tds += '</div>';
        tds += '</div>';
      tds += '</div>';

    $("#frmFacturacionTerreno .contenedorImagenesTomadas").append(tds);

    ejecutarInsert("INSERT INTO Fotos (idFoto, Ruta, Proceso, Prefijo) VALUES (?, ?, ?, ?)", [pPrefijo, imageURI, "Facturacion", publicPrefijo]);
    
    var image = document.getElementById('image_' + pPrefijo);

    image.src = imageURI;

    //uploadPhoto(imageURI, pPrefijo, Proceso);
}
function abrirArchivo_Facturacion(evento)
{
  evento.preventDefault();
  if ($("#txtFacturacion_FechaInicio").val() != "")
  {
    if ($("#txtFacturacionTerreno_Grupo").val() != "")
    {
      navigator.camera.getPicture(cameraSuccess_Facturacion, cameraFail, 
        { quality: 80,
          sourceType: 0,
        destinationType: Camera.DestinationType.NATIVE_URI,
        saveToPhotoAlbum : true });
    }
    else
    {
      Mensaje("Error", "No ha diligenciado el grupo de facturación");
    }
  } else
  {
    Mensaje("Error", "No ha diligenciado la Fecha de la auditoría");
  }
}