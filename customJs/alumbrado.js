function Alumbrado()
{
  btnAlumbrado_Reset_click();
  $(".btnAbrirBrujula").live("click", function(evento)
    {
      evento.preventDefault();
      navigator.startApp.start("com.eonsoft.Compass", function(message) {  /* success */
              console.log(message); // => OK
          }, 
          function(error) { /* error */
              alert("No se puede abrir la aplicación");
          });
    });

  $("#txtAlumbrado_Luminaria").on("change", txtAlumbrado_Luminaria_Change);
  $("#txtAlumbrado_CD").on("change", txtAlumbrado_CD_Change);
  $("#txtAlumbrado_Municipio").on("change", txtAlumbrado_CD_Change);

  $("#txtAlumbrado_TipoControl").on("change", function()
          {
           $("#txtAlumbrado_Rele option").remove();
            var tds = "";
            if ($("#txtAlumbrado_TipoControl").val() == "I")
            {
              tds = "<option value='FC'>FOTOCELDA</option>";
                      tds += "<option value='C'>CUCHILLA</option>";
            } else
            {
              tds = "<option value='CM'>CONTROL MAGNETICO</option>";
                      tds += "<option value='C'>CUCHILLA</option>";
            }
            $("#txtAlumbrado_Rele").append(tds);
          });
  
  $("#btnCameraAlumbrado").on("click", abrirCamara);
  $("#btnCameraArchivoAlumbrado").on("click", abrirArchivo);

  $("#btnAlumbrado_BorrarDatosUsuario").on("click", btnAlumbrado_BorrarDatosUsuario_Click);
  $("#btnAlumbrado_Reset").on("click", btnAlumbrado_Reset_click);

  $("#txtAlumbrado_Capa").on("change", function()
    {
      //$("#txtAlumbrado_Fase").val($("#txtAlumbrado_Capa").val());
    });

  $("#btnAlumbrado_TomarCoordenadas").on("click", function(event) 
  {
    event.preventDefault();
    ObtenerCoordenadas("txtAlumbrado_CoordenadasTrafo");
  });
  $("#btnAlumbrado_TomarCoordenadasIluminaria").on("click", function(event) 
  {
    event.preventDefault();
    ObtenerCoordenadas("txtAlumbrado_CoordenadasIluminaria");
  });
  $("#btnAlumbrado_TomarCoordenadasRele").on("click", function(event) 
  {
    event.preventDefault();
    ObtenerCoordenadas("txtAlumbrado_CoordenadasRele");
  });

  btnAlumbrado_BorrarDatosUsuario_Click();

  //$("#frmAlumbrado_DatosUsuario .chosen").chosen();
  //$("#frmAlumbrado .chosen").chosen();
  $("#grpAlumbradoFechaInicio input, #grpAlumbradoFechaTerminacion input").datepicker("destroy");
    $("#grpAlumbradoFechaInicio input, #grpAlumbradoFechaTerminacion input").datepicker(
          {
            changeMonth: true,
            changeYear: true
          });

          $("#grpAlumbradoFechaInicio .add-on").on("click", function()
          {
            $("#grpAlumbradoFechaInicio input").datepicker("show");
          });
          $("#grpAlumbradoFechaTerminacion .add-on").on("click", function()
          {
            $("#grpAlumbradoFechaTerminacion input").datepicker("show");
          });

          $("#frmAlumbrado").on("submit", function(evento)
            {
              evento.preventDefault();
              var pPrefijo = $('#txtPrefijo').val();
              datos = [Usuario.id, $('#txtPrefijo').val(), $('#txtAlumbrado_Nombre').val(), $('#txtAlumbrado_TipoOt').val(), $('#txtAlumbrado_FechaInicio').val(), $('#txtAlumbrado_Ejecutor').val(), $('#txtAlumbrado_Municipio').val(), $('#txtAlumbrado_CD').val(), $('#txtAlumbrado_CoordenadasTrafo').val(), $('#txtAlumbrado_Capa').val(), $('#txtAlumbrado_Fase').val(), $('#txtAlumbrado_Ubicacion').val(), $('#txtAlumbrado_Barrio').val(), $('#txtAlumbrado_Direccion').val(), $('#txtAlumbrado_Red').val(), $('#txtAlumbrado_CodPoste').val(), $('#txtAlumbrado_TipoControl').val(), $('#txtAlumbrado_CantIlu').val(), $('#txtAlumbrado_TipoIluminarias').val(), $('#txtAlumbrado_TipoIluminaria').val(), $('#txtAlumbrado_Propietario').val(), $('#txtAlumbrado_Via').val(), $('#txtAlumbrado_Rele').val(), $('#txtAlumbrado_TipoRed').val(), $('#txtAlumbrado_Materiales').val(), $('#txtAlumbrado_Estado').val(), $('#txtAlumbrado_TiempoOp').val(), $('#txtAlumbrado_Red2').val(), $('#txtAlumbrado_Observaciones').val(), $('#txtAlumbrado_EstadoOp').val(), $("#txtAlumbrado_DireccionLuminaria").val(), $("#txtAlumbrado_UsoRed").val(), $("#txtAlumbrado_Luminaria").val(), $("#txtAlumbrado_CoordenadasIluminaria").val(), $("#txtAlumbrado_CoordenadasRele").val()];
              var fecha = obtenerFecha();
    ejecutarInsert("UPDATE Inspecciones SET fechaFin = ?, Estado = ? WHERE Prefijo = ?", [fecha, 1, pPrefijo]);
    
    ejecutarInsert("INSERT INTO Alumbrado (idLogin, Prefijo, OT, Tipo, FechaIngreso, Ejecutor , Municipio, CD, CoorTransformador, CapaTrafo, Fase, Ubicacion, Barrio, Direccion, Red, CodPoste, TipoControl, CantLuminarias, TipoLuminarias, TipoIluminaria, Propietario, Via, TipoControl2, TipoRed, Materiales, Estado, TiempoOperacion, Red2, Observaciones, Estado2, DireccionLuminaria , UsoRed , Luminaria , CoordenadasIluminaria , CoordenadasRele) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", datos, function()
      {
        Mensaje("Ok", "El Alumbrado ha sido almacenado.");    
        $("#frmAlumbrado")[0].reset();
        $("#modulo_alumbrado").hide();
      });
            });
}
function btnAlumbrado_Reset_click()
{
   var f = new Date();
   pPrefijo = CompletarConCero(Usuario.id, 3) + f.getFullYear() + CompletarConCero(f.getMonth() +1, 2) + CompletarConCero(f.getDate(), 2) + CompletarConCero(f.getHours(), 2) + CompletarConCero(f.getMinutes(), 2) + CompletarConCero(f.getSeconds(), 2);
  $("#alumbradoSubir").attr("src" ,"subir/index.html?Prefijo=Alumbrado/" + pPrefijo);
  $("#txtAlumbrado_Prefijo").val(pPrefijo);
 
}
function btnAlumbrado_BorrarDatosUsuario_Click(evento)
{
  if (evento)
  {
    evento.preventDefault();
  }
  $("#frmAlumbrado_DatosUsuario")[0].reset();
  var f = new Date();
  pFecha = f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-" + CompletarConCero(f.getDate(), 2);

  $("#grpAlumbradoFechaInicio input").val(pFecha);
  $("#txtAlumbrado_Ejecutor").val(Usuario.nombre); 
}
function txtAlumbrado_CD_Change()
{
  var objMunicipio = $("#txtAlumbrado_Municipio").val();
  var objCD = $("#txtAlumbrado_CD").val();
  
  if (objMunicipio != '' && objCD != '')
  {
    ejecutarSQL("SELECT * FROM apTrafos WHERE municipio = ? AND cd = ? limit 0,1;", [objMunicipio, objCD],
      function(rta)
      {
        if (rta.length > 0)
        {
          var obj = rta[0];
          $("#txtAlumbrado_Nombre").val(obj.ot);
          $("#txtAlumbrado_CoordenadasTrafo").val(obj.trafo_no);
          $("#txtAlumbrado_Capa").val(obj.trafo_capa);
          $("#txtAlumbrado_Fase").val(obj.trafo_fase);
          $("#txtAlumbrado_Ubicacion").val(obj.trafo_ubic);
          $("#txtAlumbrado_Barrio").val(obj.trafo_barr);
          $("#txtAlumbrado_Direccion").val(obj.trafo_dir);
          $("#txtAlumbrado_Red").val(obj.clase_red);
          $("#txtAlumbrado_UsoRed").val(obj.uso_red);
        } else
        {
          Mensaje("Hey", "Ese Transformador no se encuentra en la bolsa descargada");
        }
      });

  }
}
function txtAlumbrado_Luminaria_Change()
{
  var objMunicipio = $("#txtAlumbrado_Municipio").val();
  var objCD = $("#txtAlumbrado_CD").val();
  var objLuminaria = $("#txtAlumbrado_Luminaria").val().toUpperCase();
  
  if (objMunicipio != '' && objCD != '' && objLuminaria != '')
  {
    ejecutarSQL("SELECT * FROM apLuminarias WHERE municipio = ? AND cd = ? AND luminaria = ? limit 0,1;", [objMunicipio, objCD, objLuminaria],
      function(rta)
      {
        if (rta.length > 0)
        {
          var obj = rta[0];
          $("#txtAlumbrado_CodPoste").val(obj.punto_sig);
          $("#txtAlumbrado_DireccionLuminaria").val(obj.ilu_dir);
          $("#txtAlumbrado_CoordenadasIluminaria").val(obj.ilu_no);
          $("#txtAlumbrado_TipoControl").val(obj.ilu_tipoco);
          $("#txtAlumbrado_CantIlu").val(obj.ilu_cant);
          $("#txtAlumbrado_TipoIluminarias").val(obj.ilu_tiplam);
          $("#txtAlumbrado_TipoIluminaria").val(obj.ilu_ilumin);
          $("#txtAlumbrado_Propietario").val(obj.ilu_propie);
          $("#txtAlumbrado_Via").val(obj.ilu_tipvia);
          $("#txtAlumbrado_Rele").val(obj.rele_tipo);
          $("#txtAlumbrado_CoordenadasRele").val(obj.rele_no);
          $("#txtAlumbrado_TipoRed").val(obj.red_tipseg);
          $("#txtAlumbrado_Materiales").val(obj.red_mat);
          $("#txtAlumbrado_Estado").val(obj.red_estado[0]);
          $("#txtAlumbrado_TiempoOp").val(obj.tiempo_op);
          $("#txtAlumbrado_Red2").val(obj.tipo_red);
          $("#txtAlumbrado_EstadoOp").val(obj.estado);
        } else
        {
          Mensaje("Hey", "Esa Luminaria no se encuentra en la bolsa descargada");
        }
      });

  }
}