function CT_INT()
{
  $("#frmCTInt .button-submit").on("click", function()
    {
      CT_Int_Obs = "";
      var obj = $("#frmCTInt .CTInt_radio");
      var idObj;
      var objTxt;
      $.each(obj, function(index, objeto)
        {
          idObj = $(objeto).attr("id").replace("CTInt_radio_", "");
          objTxt = $("#CTInt_radio_Contenedor_" + idObj).find("textarea");

          CT_Int_Obs += idObj  + "#C#" + $('input:radio[name=rad_' + idObj + ']:checked').val() + "#C#" + $(objTxt).val() + "#F#";
        });
      var idInspeccion = $("#txtPrefijo").val();

      ejecutarInsert("INSERT INTO CT_INT (Prefijo, Resultado) VALUES(?, ?)", [idInspeccion, CT_Int_Obs]);
        Mensaje("Ok", "La Interventoría ha sido almacenada.");
        $("#modulo_CT_Int").remove();

      });

    $( ".CTInt_radio" ).buttonset();

    $("#CTInt_radio_1-13 input").on("click", function()
      {
        CTInt_MoverGrupo([{Objeto: "1-14", ValorTrue : 1, ValorFalse: 2}], $("#rad_Si_1-13").is(':checked'));
      });
    
    $("#CTInt_radio_3-3 input").on("click", function()
      {
        CTInt_MoverGrupo([{Objeto: "3-4", ValorTrue : 1, ValorFalse: 0}, {Objeto: "3-5", ValorTrue : 1, ValorFalse: 0}], $("#rad_Si_3-3").is(':checked'));
      });

    $("#CTInt_radio_5-1 input").on("click", function()
      {
        if ($("#rad_No_5-1").is(':checked'))
        {
          CTInt_MoverGrupo([
            {Objeto: "5-2", ValorTrue : 0, ValorFalse: 0},
            {Objeto: "5-3", ValorTrue : 0, ValorFalse: 0},
            {Objeto: "5-4", ValorTrue : 0, ValorFalse: 0},
            {Objeto: "5-9", ValorTrue : 0, ValorFalse: 0},
            {Objeto: "5-10", ValorTrue : 0, ValorFalse: 0},
            {Objeto: "5-11", ValorTrue : 0, ValorFalse: 0},
            {Objeto: "5-12", ValorTrue : 0, ValorFalse: 0}
            ], $("#rad_No_5-1").is(':checked'));  
        }
        if ($("#rad_Si_5-1").is(':checked'))
        {
          CTInt_MoverGrupo([{Objeto: "5-2", ValorTrue : 0, ValorFalse: 0}], $("#rad_No_5-1").is(':checked'));
        }
      });

  
    $("#frmCTInt .button-submit").hide();
    $("#frmCTInt .button-previous").hide();

    $("#frmCTInt").bootstrapWizard({
        'nextSelector': '#frmCTInt_button-next',
        'previousSelector': '#frmCTInt_button-previous',
        onTabClick: function (tab, navigation, index) {
            var total = navigation.find('li').length;
            var current = index + 1;
            // set wizard title
            //App.scrollTo($('#bar'));

        },
        onNext: function (tab, navigation, index) {
            var total = navigation.find('li').length;
            var current = index + 1;
            // set done steps
        },
        onPrevious: function (tab, navigation, index) {
            var total = navigation.find('li').length;
            var current = index + 1;
            // set wizard title
        },
        onTabShow: function (tab, navigation, index) {
          $("#frmCTInt .page-title:visible").text($(tab).find(".desc").text());
            var total = navigation.find('li').length;
            //var current = index + 1;
            var li_list = navigation.find('li');
            jQuery(li_list[index]).addClass("done");
            var current = $(".done").length;
            var $percent = (current / total) * 100;
            current = index + 1;
            previusNextWizard(current, total, $("#frmCTInt"));

            $('#frmCTInt').find('.bar').css({
                width: $percent + '%'
            });
        }
    });
    function previusNextWizard(current, total, objForm)
    {
      if (current == 1) {
            $(objForm).find('.button-previous').hide();
        } else {
            $(objForm).find('.button-previous').show();
        }

        if (current >= total) {
            $(objForm).find('.button-next').hide();
            $(objForm).find('.button-submit').show();
        } else {
            $(objForm).find('.button-next').show();
            $(objForm).find('.button-submit').hide();
        }
    }
    function CTInt_MoverGrupo(objArray, valorSeleccionado)
    {
      if (valorSeleccionado)
      {
        $.each(objArray, function(index, value)
        {
          $("input:radio[name=rad_" + value.Objeto + "]")[value.ValorTrue].checked = true;
          $("#CTInt_radio_" + value.Objeto).buttonset("refresh");
          $("#CTInt_radio_Contenedor_" + value.Objeto).slideDown();
        });  
      } else
      {
        $.each(objArray, function(index, value)
        {
          $("input:radio[name=rad_" + value.Objeto + "]")[value.ValorFalse].checked = true;
          $("#CTInt_radio_" + value.Objeto).buttonset("refresh");
          $("#CTInt_radio_Contenedor_" + value.Objeto).slideUp();
        });  
      }
    }
}