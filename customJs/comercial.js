function Comercial()
{
  var f = new Date();
  pPrefijo = $("#txtPrefijo").val();
  $("#txtProceso").val("Comercial");

    $("#btnCameraComercial").on("click", abrirCamara);
    $("#btnCameraArchivoComercial").on("click", abrirArchivo);

  $(".toggle").toggleButtons({
              width: 200,
              label: {
                  enabled: "Si",
                  disabled: "No"
              }
          });

  $("#btnComercial_TomarCoordenadas").on("click", function(event) 
  {
    event.preventDefault();
    Mensaje("Ubicar", "Se han tomado coordenadas de: Carrera 11A # 93-2 a 93-100");
  });
  
  //$("#txtComercial_Ejecutor").val("admin");
  comercialValoresDefecto();

  $("#txtComercial_Nombre").on("change", cargarOrden);
  $("#frmComercial .chosen").chosen();
  $("#grpComercialFechaInicio input, #grpComercialFechaTerminacion input").datepicker("destroy");
    $("#grpComercialFechaInicio input, #grpComercialFechaTerminacion input").datepicker(
          {
            changeMonth: true,
            changeYear: true
          });

          $("#grpComercialFechaInicio .add-on").on("click", function()
          {
            $("#grpComercialFechaInicio input").datepicker("show");
          });
          $("#grpComercialFechaTerminacion .add-on").on("click", function()
          {
            $("#grpComercialFechaTerminacion input").datepicker("show");
          });

        $("#btnComercial_Reset").on("click", comercialValoresDefecto);
          $("#frmComercial").on("submit", function(evento)
            {
              evento.preventDefault();
              var datosComercial =
              [
                pPrefijo,
                Usuario.id,
                $("#txtComercial_Nombre").val(),
                $("#txtComercial_FechaInicio").val(),
                $("#txtComercial_HoraIni").val(),
                $("#txtComercial_Direccion").val(),
                $("#txtComercial_Atendio").val(),
                $("#txtComercial_Telefono").val(),
                $('#txtComercial_CruadrillaTipo').val(),
                $('#txtComercial_CuadrillaNum').val(),
                $('#txtComercial_CuadrillaTec1').val(),
                $('#txtComercial_CuadrillaAux1').val(),
                $('#txtComercial_CuadrillaAux2').val(),
                $('#txtComercial_CedulaSupervisor').val(),
                $('#txtComercial_CuadrillaCelulares').val(),
                $('#txtComercial_DetalleActividad').val(),
                $('#txtComercial_CedulaSupervisor').val(),
                $('#txtComercial_CuadrillaCedulas').val(),
                $('#txtComercial_SupervisorCelular').val(),
                $('#txtComercial_NoOrden').val(),
                $("#txtComercial_TipoInterventoria").val(),
                $("#txtComercial_SubProceso").val(),
                $("#txtComercial_Medida").val(),
                $("#txtComercial_Se").val(),
                $("#txtComercial_Medida2").val(),
                $("#txtComercial_ActividadEconomica").val(),
                $("#txtComercial_Factor").val(),
                $("#txtComercial_MedActiva").val(),
                $("#txtComercial_LecActiva").val(),
                $("#txtComercial_TipoFase").val(),
                $("#txtComercial_Marca").val(),
                $("#txtComercial_Red").val(),
                checkValor("txtComercial_AccesoMedido"),
                $("#txtComercial_TipoAcometida").val(),
                checkValor("txtComercial_AcometidaCompartida"),
                $("#txtComercial_LocalizacionMedidor").val(),
                $("#txtComercial_MedidaReferencia").val(),
                $("#txtComercial_Sellos").val(),
                $("#txtComercial_CapaTrafo").val(),
                $("#txtComercial_CD").val(),
                $("#txtComercial_PuntoFisico").val(),
                $("#txtComercial_Observaciones").val(),
                checkValor("txtComercial_FormatoSupervision"),
                $("#txtComercial_Cuadrillas").val(),
                checkValor("txtComercial_InspeccionesRealizadas"),
                $("#txtComercial_PlanSupervision").val(),
                $("#txtComercial_ObservacionesCliente").val(),
                $("#txtComercial_ObservacionesPredio").val(),
                checkValor("txtComercial_Cumple"),
                $("#txtComercial_CodIncumplimiento").val()
              ];

                  

              ejecutarInsert("INSERT INTO Comercial (Prefijo, idLogin, OT, FechaIngreso, HoraInicio, Direccion, Atendio, Telefono, CruadrillaTipo, CuadrillaNum, CuadrillaTec1, CuadrillaAux1, CuadrillaAux2, CuadrillaSupervisor, CuadrillaCelulares, DetalleActividad, CedulaSupervisor, CuadrillaCedulas, SupervisorCelular, NoOrden, TipoInterventoria, SubProceso, Medida, SE, Medida2, ActividadEconomica, Factor, MedActiva, LecActiva, TipoFase, Marca, Red, AccesoMedidor, TipoAcometida, AcometidaCompartida, LocalizacionMedidor, MedidaReferencia, Sellos, CapaTrafo, CD, PuntoFisico, Observaciones, FormatoSupervision, Cuadrillas, InspeccionesRealizadas, PlanSupervision, ObservacionesCliente, ObservacionesPredio, Cumple, CodIncumplimiento) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", datosComercial, 
                function()
                {
                  var fechaCierre = obtenerFecha();
                  ejecutarInsert("UPDATE Inspecciones SET fechaFin = ?, Estado = ? WHERE Prefijo = ?", [fechaCierre, 1, pPrefijo]);

                      Mensaje("Ok", "La Ejecución ha sido almacenada, por favor diligencie la segunda parte.");    
                      $("#frmComercial")[0].reset();
                      if ($("#txtComercial_idContrato").val() != "")
                      {
                        $("#trContrato_" + $("#txtComercial_idContrato").val()).remove();
                      }
                      $("#modulo_comercial").remove();
                      $(this).cargarModulo({pagina : "CT_Int", titulo : "Interventoría", icono : "icon-copy"});
                });

            });
}
function comercialValoresDefecto()
{
  var f = new Date();
  $("#grpComercialFechaInicio input").val(f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-" + CompletarConCero(f.getDate(), 2));
  $("#txtComercial_HoraIni").val( CompletarConCero(f.getHours(), 2) + ":" + CompletarConCero(f.getMinutes(), 2));
}

function cargarOrden()
{
  var pOt = $(this).val();
  if (pOt.length > 4)
  {
    $.post("http://fenix.wspcolombia.com/php/buscarContratos.php", 
      {parametros: "Nombre", criterio : pOt, ejecutados : "true"}, 
      function(data)
      {
        var idx = 0;
        $.each(data, function(index, value)
          {
            if (value.Estado == "Ejecutado")
            {
              Mensaje("Hey", "Esa orden ya fue ejecutada.");      
            }
              $("#txtComercial_idContrato").val(value.idContrato);
              $("#txtComercial_Nombre").val(value.Nombre);
              $("#txtComercial_Direccion").val(value.Contratista);
              $("#txtComercial_Atendio").val(value.Contratante);
              idx++;
          });
        if (idx == 0)
        {
          Mensaje("Hey", "El Numero de Orden no está en la bolsa de Pendientes."); 
        }

      }, "json").fail(function()
      {
        //Mensaje("Error", "No hay conexión con el servidor.");
      });
  }
}