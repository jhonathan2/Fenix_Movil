var Usuario = null;

$(document).on("ready", appReady);

function appReady()
{
  document.addEventListener("backbutton", function(e)
      { 
            e.preventDefault(); 
      }, false);

  $(".cargarIpal").live("click", cargarItemsIpal);

  corregirTabla("Fotos", "Estado");
  
  $("#btnCoordenadas").on("click", function()
    {
      ObtenerCoordenadas("txtCoordenadas", false);
    });
  Usuario = JSON.parse(localStorage.getItem('hesPu'));  
  cagarBarraSuperior(Usuario);

  //$("#btnEnviarCorreo").live('click', btnEnviarCorreo_Click);

  $("#tablaMensajes tr").live('click', verCorreo);

  $(this).cargarModulo({pagina : "Inicio", titulo : "Inicio", icono : "icon-dashboard"}, cargarInicio);
  
  setInterval(hacerPush, 900000);

  setInterval(desencolarFotos, 1000*60*10);

  $(".lblLinkUsuario").live("click", cargarPerfilUsuario);

  $("#tituloDelModulo h4 span").text("Inicio");
 
  $("#lblCerrarSesion").live("click", function(evento)
    {
      evento.preventDefault();
      delete localStorage.hesPu;
      window.location.replace("index.html");
    });
}

function cagarBarraSuperior(pUsuario)
{
  $("#header").load("header.html", function()
    {
      $(".username").text(pUsuario.nombre);
      $("#lblHeaderUsuario a").attr("idLogin", pUsuario.id);
      $("#FotoPerfil").attr("src", "http://fenix.wspcolombia.com/" + pUsuario.Foto);
    });
  $("#sideMenu").load("menu.html", function()
    {
      App.init();
      $("#sideMenu li").on("click", sideMenu);
    });

  hacerPush();

}
function cargarInicio()
{
  $("#btnSincronizar").on("click", sincronizarInspecciones);
  $("#btnDescargarAp").on("click", sincronizarOtAlumbrado);
  $.post('http://fenix.wspcolombia.com/php/cargarEstadisticas_Inicio.php', {idLogin : Usuario.id}, function(data, textStatus, xhr) 
            {
              $("#txtEstadisticas_Fecha span").text(data.fecha);
                $("#txtEstadisticas").slideDown();
                if (data.total == 0)
                {
                  $(".circle-stat").slideUp();
                  $("#divJhonathan").slideUp();
                  $("#txtEstadisticas h4").text("No hay actividades programadas.");
                } else
                {
                  $(".circle-stat").slideDown();
                  $("#divJhonathan").slideDown();
                  $("#txtEstadisticas h4").text("");
                }
                
                var totalRealizadas = parseInt(data.comercialRealizadas) + parseInt(data.alumbradoRealizadas) ;
                $("#txtThick_Usuarios").val(((totalRealizadas * 100)/data.total).toFixed(2));
                $("#txtThick_Empresas").val(((data.alumbradoRealizadas * 100)/data.alumbrado).toFixed(2));
                $("#txtThick_Pruebas").val(0);//data.comercial);
                $("#txtThick_Contratos").val(0);
                $("#txtThick_Ipales").val(0);

                $("#txtNum_Usuarios").text("+" + totalRealizadas + " de " + data.total + " " + ((totalRealizadas * 100)/data.total).toFixed(2) + "%");
                if (data.alumbrado > 0)
                {
                  $("#txtNum_Empresas").text("+" + data.alumbradoRealizadas + " de " + data.alumbrado + " " + ((data.alumbradoRealizadas * 100)/data.alumbrado).toFixed(2) + "%");  
                } else
                {
                  $("#txtNum_Empresas").text("+" + data.alumbradoRealizadas  + " de 0");  
                }
                if (data.comercial > 0)
                {
                  $("#txtNum_Pruebas").text("+" + data.comercialRealizadas + " de " + data.comercial + " " + ((data.comercialRealizadas * 100)/data.comercial).toFixed(2) + "%");
                } else
                {
                  $("#txtNum_Pruebas").text("+1 de 0");    
                }
                if (data.tecnica > 0)
                {
                  $("#txtNum_Contratos").text("+0 de " + data.tecnica + " 0%");  
                } else
                {
                  $("#txtNum_Contratos").text("+0 de 0");  
                }
                
                $("#txtNum_Ipales").text("+" + data.ipales);
                

            }, 'json').always(function() 
              {
                $(".knob").knob();
                //Cuando Finaliza
              }).fail(function() {
                Mensaje("Error", "No hay conexión con el servidor");
                $("#txtEstadisticas").slideUp();
                $("#txtError").slideDown();
              });
}

$.fn.cargarCorreos = function(options, callback)
      {
        var defaults =
        {
          idUsuario : "0",
          Estado: 'Pendiente',
          Tipo: ''
        }
        var options = $.extend(defaults, options);
        if (callback === undefined)
        {callback = function(){};}

      /*plugin*/
      
        $.post("http://fenix.wspcolombia.com/php/cargarCorreos.php", 
          {pUsuario : options.idUsuario, pEstado: options.Estado, pTipo : options.Tipo},
          function(data, textStatus, xhr)
          {
            callback(data);
          },"json");
      /*plugin*/

      //Averigua si el parámetro contiene una función de ser así llamarla
        if($.isFunction(options.onComplete)) 
        {options.onComplete.call();}
      };
function sideMenu(event)
{
  if (!($(this).hasClass('has-sub')))
  {
    if ($(this).text() == "Inicio")
    {
      $(this).cargarModulo({pagina : $(this).attr("pagina"), titulo : $(this).text(), icono : $(this).attr("icono")}, cargarInicio);  
    } else
    {
      $(this).cargarModulo({pagina : $(this).attr("pagina"), titulo : $(this).text(), icono : $(this).attr("icono")});
    }
    
    
  }  
}
function hacerPush (argument) 
{
  comprobarActualizacion();
  ObtenerCoordenadas("txtCoordenadas", false);
  sincronizarInspecciones();
  /*
  $("#desplegableCorreos").cargarCorreos({idUsuario: Usuario.id, Estado: 'Pendiente', Tipo : 'Recibidos'}, 
    function(Correos)
    {
      $("#desplegableCorreos li").remove();
      $(".lblContadorMensajes").hide();
      var s_ = "";

      var tds = "<li><p>Tienes <span class='lblContadorMensajes'>0</span> nuevo"+ s_ + " mensaje" + s_ +"</p></li>";
      $.each(Correos, function(index, value)
        {
          if (Correos.length > 1)
          { s_ = "s"; }

          if (index < 5)
          {
            tds += "<li><a href='#'>";
            //tds += '<span class="photo"><i class="icon-envelope"></i></span>';
            //tds += '<span class="photo"><img src="./img/avatar-mini.png" alt="avatar"></span>';
            
            tds += '<span class="subject">';
            tds += '  <span class="from">' + value.nombreRemitente + '</span><br />';

            tds += '  <span class="time">' + value.Fecha + '</span><br />';
            tds += '</span>';
            tds += '<span class="message">' + value.Asunto + '</span>';
            tds += '</a></li>';
          }
        });
      //tds += "<li><a href='inbox.html'>Ver todos los mensajes</a></li>";
      $("#desplegableCorreos").append(tds);
      if (Correos.length > 0)
      {
        $(".lblContadorMensajes").text(Correos.length);  
        $(".lblContadorMensajes").show();
      }
      
    });
  */
}

$.fn.crearTabla1 = function(options, callback)
{
  var idTabla = $(this).attr("id");
  var defaults =
  {
    lblMenu : "Registros por página"
  }
  var options = $.extend(defaults, options);
  if (callback === undefined)
  {callback = function(){};}

/*plugin*/
  
  $('#' + idTabla).dataTable().fnDestroy();
//"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",  
  tabla = $('#' + idTabla).dataTable({
            "sDom": 'CTW<"clear">lfrtip',
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ " + options.lblMenu + "",
                "oPaginate": {
                    "sPrevious": "Ant",
                    "sNext": "Sig"
                }
            },
            "oTableTools": 
            {
          "sSwfPath": "assets/data-tables/media/swf/copy_csv_xls_pdf.swf",
          "aButtons": [
              {
                "sExtends": "copy",
                "sButtonText": "Copiar"
              },
              {
                "sExtends": "csv",
                "sButtonText": "CSV"
              },
              {
                "sExtends": "xls",
                "sButtonText": "Excel"
              }
            ]
            }
        });

        jQuery('#' + idTabla + '_wrapper .DTTT').addClass("span6 offset10"); // modify table search input
        jQuery('#' + idTabla + '_wrapper .dataTables_filter input').addClass("input-medium"); // modify table search input
        jQuery('#' + options.idTabla + '_wrapper .dataTables_length select').addClass("input-mini"); // modify table per page dropdown
/*plugin*/

//Averigua si el parámetro contiene una función de ser así llamarla
  if($.isFunction(options.onComplete)) 
  {options.onComplete.call();}
};
function btnEnviarCorreo_Click(event) 
{
  event.preventDefault();
}
function verCorreo()
{
  var pIdMensaje = $(this).attr('idMensaje');
  $("#txtIdMensaje").val(pIdMensaje);
}
function Mensaje(Titulo, Mensaje)
{
  $.gritter.add({
        title: Titulo,
        text: Mensaje
      });
}
$.fn.cargarModulo = function(options, callback)
{
  var defaults =
  {
    pagina : "404",
    titulo : "Funcion no Encontrada",
    icono : "icon-ban-circle"
  };
  var options = $.extend(defaults, options);
  if (callback === undefined)
  {callback = function(){};}

/*plugin*/
    var nonModulo = options.pagina;
  nomModulo = options.pagina;
  nomModulo = "modulo_" + nomModulo.replace(/\s/g, "_");
  nomModulo = nomModulo.replace(/\./g, "_");
  nomModulo = nomModulo.replace(/\//g, "_");
  var tds = "";
  $(".widget-body").slideUp();
  $(".icon-chevron-up").removeClass("icon-chevron-up").addClass("icon-chevron-down");

  /*
  if ($("#main_menu_trigger").is(":visible"))
  {
    $("#sideMenu").slideUp();
  } */

  if ($('#' + nomModulo).length)
  {
     $('#' + nomModulo).show('slide');
     $("#" + nomModulo + " .widget-body").slideDown();
     $("#" + nomModulo + " .icon-chevron-down").removeClass("icon-chevron-down").addClass("icon-chevron-up");
     $("#" + nomModulo + " .widget-title h4 span").text(options.titulo);
     callback();
  } else
  {
    tds += '<div id="' + nomModulo + '" class="">';
    tds += '  <div class="widget">';
    tds += '        <div class="widget-title">';
    tds += '           <h4><i class="' + options.icono + '"></i><span>Cargando...</span></h4>';
    tds += '           <span class="tools">';
    tds += '           <a href="javascript:;" class="icon-chevron-up btnMinimizar"></a>';
    tds += '           <a class="icon-remove btnCerrar" style="font-size:2em;" href="javascript:;"></a>';
    tds += '           </span>';
    tds += '        </div>';
    tds += '        <div class="widget-body">';
    tds += '        </div>';
    tds += '  </div>';
    tds += '</div>';
    
    $("#contenedorModulos").append(tds);  
     var n = options.pagina.search(".html");
     if (n < 1)
     {
        options.pagina = options.pagina + ".html";
     }
     $("#" + nomModulo + " .widget-body").load(options.pagina, function()
     {
        $("#" + nomModulo + " .widget-title h4 span").text(options.titulo);
          minimizarPorlet(nomModulo);
          cerrarPorlet(nomModulo);
          callback();
     });
  }

/*plugin*/

//Averigua si el parámetro contiene una función de ser así llamarla
  if($.isFunction(options.onComplete)) 
  {options.onComplete.call();}
};

function minimizarPorlet(id)
{
  $("#" + id + " .widget-title").on("click", function(event) 
  {
      var el = $("#" + id + " .widget-body");
            var objTitulo = $("#" + id + " .btnMinimizar");
            if (jQuery(objTitulo).hasClass("icon-chevron-down")) {
                jQuery(objTitulo).removeClass("icon-chevron-down").addClass("icon-chevron-up");
                el.slideDown(200);
            } else {
                jQuery(objTitulo).removeClass("icon-chevron-up").addClass("icon-chevron-down");
                el.slideUp(200);
            }
  });
}
function cerrarPorlet(id)
{
  jQuery('#' + id + ' .btnCerrar').on("click", function () 
    {
        jQuery('#' + id).hide('slide');
    });
}

HTTP_GET_VARS=new Array();
strGET=document.location.search.substr(1,document.location.search.length);
if(strGET!='')
    {
    gArr=strGET.split('&');
    for(i=0;i<gArr.length;++i)
        {
        v='';vArr=gArr[i].split('=');
        if(vArr.length>1){v=vArr[1];}
        HTTP_GET_VARS[unescape(vArr[0])]=unescape(v);
        }
    }

function GET(v) {
  if(!HTTP_GET_VARS[v]){return 'undefined';}
  return HTTP_GET_VARS[v];
}

function cargarPerfilUsuario()
{
  var pIdLogin = $(this).attr("idLogin");
  var pTexto = $(this).text();;
 $(this).cargarModulo({pagina : "profile", titulo : "Perfil de " + pTexto, icono : "icon-user"}, function()
 {
    cargarProfile(pIdLogin);
 }); 
}
function cargarArchivos(titulo, idCarpeta)
{
  $(this).cargarModulo({pagina : "verArchivos", titulo : "Archivos de " + titulo, icono : "icon-copy"}, function()
      {
        if($('#verArchivos').elfinder('instance'))
        { $('#verArchivos').elfinder('destroy');}
       
        var elf = $('#verArchivos').elfinder({
              url : 'http://fenix.wspcolombia.com/Archivos/php/connector.php?Contrato=' + idCarpeta,
              lang: 'es',  
              handlers:
              {
                upload : function(event) 
                    { 
                      
                    },
                open: function(event)
                    { 
                      
                    },
                rm : function(event)
                    {
                      
                    }
              }           
        }).elfinder('instance');
      });
}
function cargarArchivos_Usuario_Click()
{
  cargarArchivos($(this).attr("Nombre"), "Usuarios\/" + $(this).attr("idLogin"));
}
function CompletarConCero(n, length)
{
   n = n.toString();
   while(n.length < length) n = "0" + n;
   return n;
}
function fechasRango(desde, hasta)
{
  $( desde ).datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 2,
          onClose: function( selectedDate ) {
            $( hasta ).datepicker( "option", "minDate", selectedDate );
          }
        });
        $( hasta ).datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 2,
          onClose: function( selectedDate ) {
            $( desde ).datepicker( "option", "maxDate", selectedDate );
          }
        });
}
function checkValor(idObjeto)
{
  return $("#"+ idObjeto).is(":checked");
}
function ObtenerCoordenadas(idControl, pMensaje)
{
  var objCoordenadas ="";
  $("#" + idControl).val(objCoordenadas); 
  navigator.geolocation.getCurrentPosition(
    function(datos)
    {
      var lat = datos.coords.latitude;
      var lon = datos.coords.longitude;
      var accu = datos.coords.accuracy;

      objCoordenadas =  lat + "," + lon + "#" + accu;
        if (pMensaje != false)
        {
            Mensaje("Ubicación", objCoordenadas);
        }
      $("#" + idControl).val(objCoordenadas);
    }, 
    function ()
    {
      objCoordenadas ="No hay precision en el dato";
      if (pMensaje != false)
      {
        Mensaje("Ubicación", objCoordenadas);
      }
    });
  return objCoordenadas;
}
function comprobarActualizacion()
{

}
function rgbToHex(R,G,B) 
{
  return toHex(R)+toHex(G)+toHex(B);
}
function toHex(n) 
{
   n = parseInt(n,10);
   if (isNaN(n)) return "00";
   n = Math.max(0,Math.min(n,255));
   return "0123456789ABCDEF".charAt((n-n%16)/16) + "0123456789ABCDEF".charAt(n%16);
}
function obtenerFecha()
{
  var f = new Date();
    return f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-" + CompletarConCero(f.getDate(), 2) + " " + CompletarConCero(f.getHours(), 2) + ":" + CompletarConCero(f.getMinutes(), 2) + ":" + CompletarConCero(f.getSeconds(), 2);
}
function abrirCamara(evento)
{
  evento.preventDefault();
  navigator.camera.getPicture(cameraSuccess, cameraFail, 
    { quality: 80,
      sourceType: 1,
    destinationType: Camera.DestinationType.FILE_URI,
    saveToPhotoAlbum : true });
}

function cameraSuccess(imageURI) 
{
    var pPrefijo = obtenerPrefijo();
    var publicPrefijo = $("#txtPrefijo").val();
    var publicProceso = $("#txtProceso").val();

    var tds = '<div class="span4">';
      tds += '<div class="thumbnail">';
        tds += '<div class="item">';
              tds += '<img src="" alt="Photo" id="image_' + pPrefijo + '"/>';
        tds += '</div>';
      tds += '</div>';
    tds += '</div>';

    var Objeto = "";
    if (publicProceso == "Ipales")
    {
      Objeto = "frmIpal";
    }
    if (publicProceso == "Comercial")
    {
      Objeto = "frmComercial";
    }
    if (publicProceso == "Alumbrado")
    {
      Objeto = "frmAlumbrado";
    }
    $("#" + Objeto + " .contenedorImagenesTomadas").append(tds);

    ejecutarInsert("INSERT INTO Fotos (idFoto, Ruta, Proceso, Prefijo) VALUES (?, ?, ?, ?)", [pPrefijo, imageURI, publicProceso, publicPrefijo]);
    
    var image = document.getElementById('image_' + pPrefijo);

    image.src = imageURI;

    //uploadPhoto(imageURI, pPrefijo, Proceso);
}

function cameraFail(message) 
{
    alert('Failed because: ' + message);
}

function uploadPhoto(imageURI, Prefijo, idFoto) 
{
  var options = new FileUploadOptions();
  options.fileKey="file";
  options.fileName=imageURI.substr(imageURI.lastIndexOf('/')+1);
  options.mimeType="image/jpeg";
  
  var params = {};
  params.value1 = "test";
  params.value2 = "param";
  //alert("Se va a enviar la Foto: " + idFoto);

  
  if (imageURI.substr(0, 7) == "content")
  {
    window.requestFileSystem(LocalFileSystem.TEMPORARY, 0, function(r)
      {      }, 
      function(error)
      {      //alert("Reques: "+ error.code);
      });
     window.resolveLocalFileSystemURI(imageURI, function(fileEntry) 
     {
      imageURI = fileEntry.toURL();
      //alert("La url se convirtió a: " + imageURI);
     }, function()
     {
        //alert("resolve: "+ error.code);
        ejecutarInsert("UPDATE Fotos SET idFoto = ? WHERE idFoto = ?", ["$" + idFoto, idFoto]);
        ejecutarInsert("UPDATE Fotos SET Estado = NULL WHERE idFoto = ?", [idFoto]);
     });      
  }
  
  options.params = params;
  
  var ft = new FileTransfer();
  //alert("Se va a subir a: " + Prefijo);
  ft.upload(imageURI, "http://fenix.wspcolombia.com/subir/uploadF.php?Ruta=" + Prefijo, function(r)
    {
      //alert("Response: " + r.response);
      if (r.response == 1)
      {
        ejecutarInsert("DELETE FROM Fotos WHERE idFoto = ?", [idFoto]);
        bucleFotos();
      }  else
      {
        ejecutarInsert("UPDATE Fotos SET Estado = NULL WHERE idFoto = ?", [idFoto]);
      }
    }, function(error)
    {
      //alert("Upload error " + error.code);
      if (error.code == 1)
      {
        ejecutarInsert("UPDATE Fotos SET idFoto = ? WHERE idFoto = ?", ["$" + idFoto, idFoto]);
        ejecutarInsert("UPDATE Fotos SET Estado = NULL WHERE idFoto = ?", [idFoto]);
        bucleFotos();
      }
    }, options);
}
function obtenerPrefijo()
{
  var f = new Date();
    return CompletarConCero(Usuario.id, 3) + f.getFullYear() + CompletarConCero(f.getMonth() +1, 2) + CompletarConCero(f.getDate(), 2) + CompletarConCero(f.getHours(), 2) + CompletarConCero(f.getMinutes(), 2) + CompletarConCero(f.getSeconds(), 2);
}
function sincronizarInspecciones(evento)
{
  $("#lblSincronizando").slideDown();
  if (evento === undefined)
  {

  } else
  {
    evento.preventDefault();
  }
  

  ejecutarSQL('SELECT * FROM Fotos', [], 
  function(fotos)
  {
    $("#lblFotos strong").text(fotos.length);
    bucleFotos();
  });
  
  ejecutarSQL('SELECT * FROM inspecciones', [], 
    function(inspecciones)
    {
      $("#lblEncuestas strong").text(inspecciones.length);

      if (inspecciones.length > 0)
      {
        enviarServidor("crearInspeccion.php", inspecciones, function(data) 
        {
              var fecha = obtenerFecha();
              $("#lblUltimaActualizacion small").text(fecha);
            if (data == 1)
            {
              $("#lblEncuestas strong").text(0);
            }
        });
      }
    });

  ejecutarSQL('SELECT * FROM Ipal', [], 
  function(Ipales)
  {
    $("#lblIpales strong").text(Ipales.length);
    if (Ipales.length > 0)
      {
        enviarServidor("crearIpal.php", Ipales, function(data) 
        {

        });
      }
  });

  ejecutarSQL('SELECT * FROM Comercial', [], 
  function(Comercial)
  {
    $("#lblComercial strong").text(Comercial.length);
    
    if (Comercial.length > 0)
      {
        
        enviarServidor("crearComercial.php", Comercial, function(data) 
        {

        });
      }
  });

  ejecutarSQL('SELECT * FROM Alumbrado', [], 
  function(Alumbrado)
  {
    $("#lblAlumbrado strong").text(Alumbrado.length);
    
    if (Alumbrado.length > 0)
      {
        enviarServidor("crearAlumbrado.php", Alumbrado, function(data) 
        {

        });
      }
  });


ejecutarSQL('SELECT * FROM Facturacion', [], 
  function(FacturacionTerreno)
  {
    if (FacturacionTerreno.length > 0)
      {
        $.each(FacturacionTerreno, function(index, val) 
        {
          enviarServidor("crearFacturacion.php", [val], function(data) 
          {
            
          });
        });
      }
  });

  ejecutarSQL('SELECT * FROM FacturacionTerreno', [], 
  function(FacturacionTerreno)
  {
    $("#lblFacturacion strong").text(FacturacionTerreno.length);
    
    if (FacturacionTerreno.length > 0)
      {
        $.each(FacturacionTerreno, function(index, val) 
        {
          enviarServidor("crearFacturacionTerreno.php", [val], function(data) 
          {
            
          });
        });
      }
  });

  $("#lblSincronizando").slideUp();

  ejecutarSQL("SELECT Prefijo FROM Inspecciones", [], 
  function(Inspecciones)
  {
    $("#lblEncuestas strong").text(Inspecciones.length);
    
    if (Inspecciones.length > 0)
      {
        $.post('http://fenix.wspcolombia.com/movil2/comprobarInspeccion.php', {datos: Inspecciones, tipo : "Inspecciones"}, 
          function(data, textStatus, xhr) 
          {
            $.each(data, function(index, val) 
            {
              ejecutarSQL("DELETE FROM Inspecciones WHERE Prefijo = ?", [val.Prefijo]);
            });
          }, "json");
      }
  });

  ejecutarSQL("SELECT Prefijo FROM Comercial", [], 
  function(Inspecciones)
  {
    $("#lblComercial strong").text(Inspecciones.length);
    
    if (Inspecciones.length > 0)
      {
        $.post('http://fenix.wspcolombia.com/movil2/comprobarInspeccion.php', {datos: Inspecciones, tipo : "Comercial_1"}, 
          function(data, textStatus, xhr) 
          {
            $.each(data, function(index, val) 
            {
              ejecutarSQL("DELETE FROM Comercial WHERE Prefijo = ?", [val.Prefijo]);
            });
          }, "json");
      }
  });

  ejecutarSQL("SELECT Prefijo FROM Ipal", [], 
  function(Inspecciones)
  {
    $("#lblIpales strong").text(Inspecciones.length);
    
    if (Inspecciones.length > 0)
      {
        $.post('http://fenix.wspcolombia.com/movil2/comprobarInspeccion.php', {datos: Inspecciones, tipo : "Ipal"}, 
          function(data, textStatus, xhr) 
          {
            $.each(data, function(index, val) 
            {
              ejecutarSQL("DELETE FROM Ipal WHERE Prefijo = ?", [val.Prefijo]);
            });
          }, "json");
      }
  });

  ejecutarSQL("SELECT Prefijo FROM Alumbrado", [], 
  function(Inspecciones)
  {
    $("#lblAlumbrado strong").text(Inspecciones.length);
    
    if (Inspecciones.length > 0)
      {
        $.post('http://fenix.wspcolombia.com/movil2/comprobarInspeccion.php', {datos: Inspecciones, tipo : "Alumbrado_1"}, 
          function(data, textStatus, xhr) 
          {
            $.each(data, function(index, val) 
            {
              ejecutarSQL("DELETE FROM Alumbrado WHERE Prefijo = ?", [val.Prefijo]);
            });
          }, "json");
      }
  });

    
    var f = new Date();
    fecha = new Date(f.getTime() - (7 * 24 * 3600 * 1000));
    fecha = (fecha.getFullYear() + "-" + CompletarConCero(fecha.getMonth() +1, 2) + "-" + CompletarConCero(fecha.getDate(), 2));

  ejecutarSQL("SELECT Prefijo FROM Facturacion WHERE Fecha < ?", [fecha], 
  function(Inspecciones)
  {
    if (Inspecciones.length > 0)
      {
        $.post('http://fenix.wspcolombia.com/movil2/comprobarInspeccion.php', {datos: Inspecciones, tipo : "Facturacion"}, 
          function(data, textStatus, xhr) 
          {
            $.each(data, function(index, val) 
            {
              ejecutarSQL("DELETE FROM Facturacion WHERE Prefijo = ? AND Fecha < ?", [val.Prefijo, fecha]);
            });
          }, "json");
      }
  });

  ejecutarSQL("SELECT PrefijoIns FROM FacturacionTerreno", [], 
  function(Inspecciones)
  {
    $("#lblFacturacion strong").text(Inspecciones.length);
    
    if (Inspecciones.length > 0)
      {
        $.post('http://fenix.wspcolombia.com/movil2/comprobarInspeccion.php', {datos: Inspecciones, tipo : "FacturacionTerreno"}, 
          function(data, textStatus, xhr) 
          {
            $.each(data, function(index, val) 
            {
              ejecutarSQL("DELETE FROM FacturacionTerreno WHERE PrefijoIns = ?", [val.Prefijo]);
            });
          }, "json");
      }
  });
}
function enviarServidor(ruta, datos, callback, callError, callFail)
{
  if (ruta != "")
  {
    if (datos === undefined)
    {    datos = {};  }
  
    if (callback === undefined)
    {    callback = function(){};  }
    
    if (callError === undefined)
    {    callError = function(text){};  }
    
    if (callFail === undefined)
    {    callFail = function(){};  }

    $.post('http://fenix.wspcolombia.com/movil2/' + ruta, {datos: datos}, 
      function(data, textStatus, xhr) 
      {
        if (data == 1)
        {
          callback(data);
        } else
        {
          callError(data);
        }
      }).fail(
        function()
        {
          callFail();
        });
  }
}


function sincronizarOtAlumbrado(evento)
{
  evento.preventDefault();
  ejecutarSQL("SELECT valor from configuracion WHERE dato = ?", ['fechaAp'],
    function(rta)
    {

      if (rta.length > 0)
      {
        Mensaje("Hey", "Va a Iniciar la descarga a partir de: " + rta[0].valor);

        $.post("http://fenix.wspcolombia.com/movil2/sincronizarOtAp.php", {fecha : rta[0].valor},
          function(data)
          {
            Mensaje("Hey", "Ya inicio la conexión con el Servidor");
            
            var pFecha = obtenerFecha();

            var obj ={};

            if (typeof(data) == "object")
            {
              ejecutarInsert("UPDATE configuracion SET valor = ? WHERE dato = ?", [pFecha, 'fechaAp']);

              $.each(data.Trafos, function(index, value)
              {
                obj = [value.municipio, value.cd, value.ot, value.trafo_no, value.trafo_mun, value.trafo_capa, value.trafo_fase, value.trafo_ubic, value.trafo_barr, value.trafo_dir, value.clase_red, value.uso_red, value.Fecha];
                ejecutarInsert("INSERT INTO apTrafos (municipio, cd, ot, trafo_no, trafo_mun, trafo_capa, trafo_fase, trafo_ubic, trafo_barr, trafo_dir, clase_red, uso_red, Fecha) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", obj);
              });

              $.each(data.Luminarias, function(index, value)
              {
                obj = [value.municipio, value.cd, value.luminaria, value.punto_sig, value.ilu_dir, value.ilu_no, value.ilu_tipoco, value.ilu_cant, value.ilu_tiplam, value.ilu_ilumin, value.ilu_propie, value.ilu_tipvia, value.rele_tipo, value.rele_no, value.red_tipseg, value.red_mat, value.red_estado, value.tiempo_op, value.tipo_red, value.estado, value.Fecha];
                ejecutarInsert("INSERT INTO apLuminarias (municipio, cd, luminaria, punto_sig, ilu_dir, ilu_no, ilu_tipoco, ilu_cant, ilu_tiplam, ilu_ilumin, ilu_propie, ilu_tipvia, rele_tipo, rele_no, red_tipseg, red_mat, red_estado, tiempo_op, tipo_red, estado, Fecha) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", obj);
              });
            }
              Mensaje("Hey", "La base de datos quedó actualizada");
          }, "json").fail(function()
          {
            Mensaje("Error", "No hay conexión con el servidor");
          });
      } 
    });
}

function bucleFotos()
{
  ejecutarSQL('SELECT * FROM Fotos WHERE idFoto NOT LIKE ?', ["$%"],
    function(rs)
    {
      $("#lblFotos strong").text(rs.length);
    });

    ejecutarSQL("SELECT * FROM Fotos WHERE idFoto NOT LIKE ? AND Estado LIKE ?", ["$%", "Enviando"],
    function(fotosEnviando)
    {
      if (fotosEnviando.length < 3)
      {
          ejecutarSQL('SELECT * FROM Fotos WHERE idFoto NOT LIKE ? AND (Estado NOT LIKE ? OR Estado IS NULL) limit 0, 1', ["$%", "Enviando"],
          function(fotos)
          {
            $.each(fotos, function(index, value)
              {
                ejecutarInsert("UPDATE Fotos SET Estado = ? WHERE idFoto = ?", ["Enviando", value.idFoto]);
                uploadPhoto(value.Ruta, value.Proceso + "/" + value.Prefijo, value.idFoto);
              });
          });
      }
    });
}
function corregirTabla(Tabla, Campo)
{
    ejecutarSQL("SELECT " + Campo + " FROM " + Tabla + ";", [], 
    function()
    {

    }, 
    function()
    {
      ejecutarSQL("ALTER TABLE " + Tabla + " ADD " + Campo + ";");
    });
}
function desencolarFotos()
{
  ejecutarSQL("UPDATE Fotos SET Estado = NULL;", [], 
    function()
    {
      
    }, 
    function()
    {
      
    });
}