$(document).on("ready", appReady);

function appReady()
{
  if (localStorage.hesPu != null)
  {
    window.location.replace("home.html");
  }
  $("#loginform").on('submit', frmSignIn_submit);

  $.post("http://fenix.wspcolombia.com/fenixMovil/sincronizarUsuarios.php", {},
    function(data)
    {
      if (typeof(data) == "object")
      {  
        ejecutarInsert("DELETE FROM Login", [], 
          function()
          {
            $.each(data, function(index, val) 
            {
               ejecutarInsert("INSERT INTO Login (id, username, password, nombre, email, cargo, Foto, empresa) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", [val.id, val.username, val.password, val.nombre, val.email, val.cargo, val.Foto, val.empresa]);
            });
          });
      } else
      {
        
      }
    }, "json");
}
function frmSignIn_submit(event)
{
  event.preventDefault();
  var cDate = new Date();

      var pUsuario = $("#txtUsuario").val();
      var pClave = md5(md5(md5($("#txtClave").val())));
      
        ejecutarSQL('SELECT * FROM Login WHERE username = ? AND password = ?', [pUsuario, pClave], 
        function(rs)
        { 
          if (rs.length != 0)
          {
            rs[0].cDate = cDate;
            localStorage.setItem("hesPu", JSON.stringify(rs[0]));  

            window.location.replace("home.html");
          } else
          {
            $(".alert").html("<strong>Error!</strong> Acceso denegado.");
            $(".alert").fadeIn(300).delay(2600).fadeOut(600);
          }
        });
    //});
}