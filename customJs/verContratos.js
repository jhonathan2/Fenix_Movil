function verContratos()
{       
   $(".btnVerContrato_Archivos").live("click", btnVerContrato_Archivos_Click);
   $(".btnVerContrato_Detalles").live("click", btnVerContrato_Detalles_Click);
   $(".btnVerContrato_Empresas").live("click", btnVerContrato_Empresas_Click);
   $(".btnEmpresas_Contratos_Borrar").live("click", btnEmpresas_Contratos_Borrar_Click);
   

   $("#txtVerContratos_BuscarPor").on("change", txtVerContratos_BuscarPor_Click);

   $("#btnVerContratos_Buscar").on("click", btnVerContratos_Buscar_Click);
}

function btnVerContrato_Archivos_Click()
{
    var Contrato = $(this).parent("div").parent("div").find("h4");
    var idContrato = $(this).attr("idContrato");

    cargarArchivos($(Contrato).text(), "OT\/" + idContrato);
}
function cerrarPorlet_()
 {
      $("#txtVerContratos_ContenedorResultados .icon-chevron-down").on("click", ocultarPortlet);

     jQuery('#txtVerContratos_ContenedorResultados .icon-remove').on ("click", function () {
        jQuery(this).parent(".tools").parent(".widget-title").parent(".widget").remove();
    });
}
function ocultarPortlet()
{
    var obj = $(this).parent(".tools").parent(".widget-title").parent(".widget").find(".widget-body");
    if (jQuery(this).hasClass("icon-chevron-down")) {
        jQuery(this).removeClass("icon-chevron-down").addClass("icon-chevron-up");
        obj.slideUp(200);
    } else {
        jQuery(this).removeClass("icon-chevron-up").addClass("icon-chevron-down");
        obj.slideDown(200);
    }
}
function txtVerContratos_BuscarPor_Click(evento)
{

  $("#txtVerContratos_Parametros").val($(this).val());
}

function btnVerContratos_Buscar_Click()
 {
    $(".icoCargando").show();
    var pParametros = $("#txtVerContratos_BuscarPor").val();
    if (pParametros != "")
    {
        $.post("http://fenix.wspcolombia.com/php/buscarContratos.php", {parametros: pParametros, criterio : $("#txtVerContratos_Criterio").val(), ejecutados : checkValor("txtVerContratos_Ejecutadas")}, function(data)
        {
          if (data == 0)
          {
            Mensaje("Hey", "No se encontraron resultados");
          } else
          {
            $("#tblOts").dataTable().fnDestroy();
            $("#tblOts tbody tr").remove();

            var tds = "";
            $.each(data, function(index, value)
              {
                tds += '<tr id="trContrato_' + value.idContrato + '">';
                tds +=  '<td><div class="btn-group">';
                tds +=    '<button class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="icon-cogs"> </i><span class="caret"></span></button>';
                tds +=    '<ul class="dropdown-menu">';
                tds +=      '<li class="btn-large btnVerContrato_Archivos" idContrato="' + value.idContrato + '"><a href="#"><i class="icon-folder-open icon-white"></i> Archivos</a></li>';
                tds +=      '<li class="btn-large btnVerContrato_Detalles"><a href="#"><i class="icon-search icon-white"></i> Detalles</a></li>';
                tds +=      '<li class="btn-large btnVerContrato_Empresas" Coordenadas="' + value.Coordenadas + '"><a href="#"><i class="icon-map-marker icon-white"></i> Como Llegar</a></li>';
                tds +=    '</ul>';
                tds +=  '</div></td>';
                tds += '<td>' + value.Nombre + '</td>';
                tds += '<td>' + value.Descripcion + '</td>';
                tds += '<td>' + value.Responsabilidades + '</td>';
                tds += '<td>' + value.Objeto + '</td>';
                tds += '<td>' + value.Contratante + '</td>';
                tds += '<td>' + value.Contratista + '</td>';
                tds += '</tr>';

              });
            $("#tblOts tbody").append(tds);
            $("#tblOts").crearTabla1({lblMenu : "Tareas por página"});
          }

        }, 'json').always(function() 
        {
          //Cuando Finaliza
          $(".icoCargando").hide();
        }).fail(function() {
          Mensaje("Error", "No fue conectar con el Servidor");
        });
    } else
    {
      Mensaje("Hey", "Tienes que seleccionar por lo menos un parámetro");
    }
      
}
function btnVerContrato_Detalles_Click()
{
  var Contrato = $(this).parent("div").parent("div").find("h4");
  var pIdContrato = $(Contrato).attr("idContrato");
   $(this).cargarModulo({pagina : "editarContrato", titulo : "Editar " + $(Contrato).text() ,icono : "icon-edit"}, function()
    {
      cargarDatosContrato(pIdContrato);
    });
}

function btnVerContrato_Empresas_Click()
{
  var pCoordenadas = $(this).attr("Coordenadas");
  if (pCoordenadas != "")
  { 
      popupWin = window.open("https://www.google.com/maps/dir/" + $("#txtCoordenadas").val() + "/" + pCoordenadas , 'open_window');    
  } else
  {
    Mensaje("Hey", "Esta orden no tiene Coordenadas");
  }
}
function btnEmpresas_Contratos_Borrar_Click()
{
  var objFila = $(this).parent("td").parent("tr").find("td");
  var idContrato = $(this).parent("td").parent("tr").parent("tbody").parent("table").attr("idContrato");
  //$(objFila).parent("tr").remove();
  var table = $('#tblEmpresas_Contratos').dataTable();
  //console.log(table);
  console.log($(this).parent('td').parent("tr"));
    //table.row( $(this).parents('tr') ).remove();  
    /*
  alert(idContrato);
  alert($(objFila[0]).text());
  */
}